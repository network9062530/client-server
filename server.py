import socket

with open("file.txt","r") as file:
    contents_file = file.read()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("", 210))

server.listen(1)
print("Server wait for accepted!!")
client_socket, client_address = server.accept()
print(f"Accepted.. form : {client_address}")
client_socket.sendall(contents_file.encode())

client_socket.close()
server.close()