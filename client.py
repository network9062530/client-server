import socket

host_target = "192.168.43.137"
port = 210

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((host_target, port))

contents_file = ""
while True:
    data = client.recv(1024).decode()
    if not data:
        break
    contents_file += data

client.close()
print("Received!!")
print(contents_file)